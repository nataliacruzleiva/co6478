<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tabla Productos</title>
    <style>
        table tr:nth-child(even) {
            background-color:#8FBC8F;
        }
        table tr:nth-child(odd) {
            background-color:white;
        }
        #a {
            background-color:#A9A9A9;
        }
        #b {
            background-color:yellow;
        }
    </style>
</head> 
<body>
    <table>
        <tr id="b">
            <th>Tabla Productos</th> 
        </tr>
         <tr id="a">
            <th>Nombre</th>
            <th>Cantidad</th>
            <th>Precio</th>
        </tr>

        <?php
            class Producto{ 
                public $nombre;
                public $cantidad;
                public $precio;

                function __construct($nombre,$cantidad,$precio) {
                    $this->nombre = $nombre;
                    $this->cantidad = $cantidad;
                    $this->precio = $precio;
                }
            }

            $cocaCola = new Producto("Coca Cola","100","4.500");
            $pepsi = new Producto("Pepsi","30","4.800");
            $sprite = new Producto("Sprite","20","4.500");
            $guarana = new Producto("Guarana","200","4.500");
            $sevenUp = new Producto("Seven Up","24","4.800");
            $mirindaNaranja = new Producto("Mirinda Naranja","56","4.800");
            $mirindaGuarana = new Producto("Mirinda Guarana","89","4.800");
            $fantaNaranja = new Producto("Fanta Naranja","10","4.500");
            $fantaPinha = new Producto("Fanta Piña","2","4.500");

            $productos = array($cocaCola,$pepsi,$sprite,$guarana,$sevenUp,$mirindaNaranja,$mirindaGuarana,$fantaNaranja,$fantaPinha);

        ?>
       
        <?php foreach ($productos as $producto) { ?> 

            <tr>
                <td><?php echo $producto->nombre; ?></td>
                <td><?php echo $producto->cantidad; ?></td>
                <td><?php echo $producto->precio; ?></td>
             </tr>
        <?php 
        }?>
    </table> 
</body>
</html>