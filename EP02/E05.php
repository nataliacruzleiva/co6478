<style>
table tr:nth-child(even) {
    background-color:white;
}
table tr:nth-child(odd) {
    background-color:gray;
}
</style>
<?php 
    echo "<table border='1'><br>";
    echo "<h2>Tabla del 9</h2>";

    for ($i=1; $i<=12 ; $i++) { 
        $valor = $i*9;
        if ($i % 2 == 0) {
            $color = 'red';
        } else {
            $color = 'blue';
        }   

        echo "<tr>";
        echo "<td>$i x 9</td>";
        echo "</tr>";
    }

    echo "</table>";
?>