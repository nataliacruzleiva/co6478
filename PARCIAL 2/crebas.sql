/*==============================================================*/
/* Table: ALUMNOS                                            */
/*==============================================================*/
create table ALUMNOS  (
   ID_ALUMNO	SERIAL               not null,
   NOMBRE 	VARCHAR(100)         null,
   APELLIDO  	VARCHAR(100)         null,
   EDAD 	INTEGER			null,
   constraint PK_ALUMNOS  primary key (ID_ALUMNO)
);

/*==============================================================*/
/* Index: ALUMNOS_PK                                          */
/*==============================================================*/
create unique index ALUMNOS_PK on ALUMNOS (
ID_ALUMNO
);


INSERT INTO ALUMNOS(NOMBRE,APELLIDO,EDAD) VALUES ('Natalia','Cruz',20);
INSERT INTO ALUMNOS(NOMBRE,APELLIDO,EDAD) VALUES ('Esteban','Espinola',20);
INSERT INTO ALUMNOS(NOMBRE,APELLIDO,EDAD) VALUES ('Karen','Amarilla',25);
INSERT INTO ALUMNOS(NOMBRE,APELLIDO,EDAD) VALUES ('Manuel','Cruz',28);
INSERT INTO ALUMNOS(NOMBRE,APELLIDO,EDAD) VALUES ('Anibal','Mexico',24);
INSERT INTO ALUMNOS(NOMBRE,APELLIDO,EDAD) VALUES ('Bernardo','Cruz',23);
INSERT INTO ALUMNOS(NOMBRE,APELLIDO,EDAD) VALUES ('Maria','Leiva',19);
INSERT INTO ALUMNOS(NOMBRE,APELLIDO,EDAD) VALUES ('Alejandro','Kunzle',28);
INSERT INTO ALUMNOS(NOMBRE,APELLIDO,EDAD) VALUES ('Omar','Marin',20);
INSERT INTO ALUMNOS(NOMBRE,APELLIDO,EDAD) VALUES ('Belen','Lopez',21);
INSERT INTO ALUMNOS(NOMBRE,APELLIDO,EDAD) VALUES ('Maria','Gonzalez',19);
INSERT INTO ALUMNOS(NOMBRE,APELLIDO,EDAD) VALUES ('Juana','Velazquez',30);







