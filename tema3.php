<?php
    class Libro 
    {
        private string $titulo;
        private $tipo;
        private string $editorial;
        private int $anho;
        private $ISBN;

        private $autores = array();
        //constructor
        public function __construct(string $titulo, $tipo, string $editorial, int $anho, $ISBN, $autores ) {
            $this->titulo = $titulo;
            $this->tipo = $tipo;
            $this->editorial = $editorial;
            $this->anho = $anho;
            $this->ISBN = $ISBN;
            $this->autores = $autores;
        }

        public function getTitulo() {
            return $this->titulo;
        }

        public function getTipo() {
            return $this->tipo;
        }

        public function getEditorial() {
            return $this->editorial;
        }

        public function getAnho() {
            return $this->anho;
        }

        public function getISBN() {
            return $this->ISBN;
        }

        public function getAutores() {
            return $this->autores;
        }
    }

    class Autor
    {
        private string $nombre;
        private string $nacionalidad;
        private $f_nacimiento;

        public function __construct(string $nombre, string $nacionalidad, $f_nacimiento) {
            $this->nombre = $nombre;
            $this->nacionalidad = $nacionalidad;
            $this->f_nacimiento = $f_nacimiento;
        }
    
        public function getNombre() {
            return $this->nombre;
        }
        public function getNacionalidad() {
            return $this->nacionalidad;
        }
        public function getF_nacimiento() {
            return $this->f_nacimiento;
        }
    }

    $autor_1 = new Autor("Natalia Cruz", "Paraguayo", date('d-m-y', strtotime('20-04-1996')));
    $autor_2 = new Autor("Bernardo Cruz Leiva", "Chileno", date('d-m-y', strtotime('20-03-1989')));

    $libro_1 = new Libro("COMO SOBREVIVIR A LA FACULTAD", "PrimerTipo", "EDITORIAL libro 1", 1996, "0123456789", array($autor_1,$autor_2));

    $listaAutores = $libro_1->getAutores();
    $nombreLibro= $libro_1->getTitulo();

    print("Los autores del libro $nombreLibro son: \n");
    
    foreach ($listaAutores as $key => $value) {
        $nombreAutor = $value->getNombre();
        print("$nombreAutor -\n");
    }
?>