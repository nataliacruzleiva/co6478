<?php
    $dbconn = new PDO("pgsql:host=localhost port=5432 dbname=finalSegunda", "postgres", "postgrenombres");
    if (!$dbconn){
        echo "ocurrio un error";
        exit;
    }

    /*falta ver si es la forma de hacerlo o directamente en el query)???
    $nombre = array(
            'Johnatan','Antonio','Elias','Nancy','Tamara','Camila','Augustina','Diego','Tomas','Alejandro','Marcos','Joaquin'
    );
    $num = 12;
    $keys = array_rand($nombre,$num);
    echo $nombre[$keys[0]];  */

    function insertAlumnos($dbconn) {   

        $nombre = array('Johnatan','Antonio','Elias','Nancy','Tamara','Camila','Augustina','Diego','Tomas','Alejandro');
        $apellido = array('Espinola','Cruz','Martinez','Lopez','Moreira','Paredes','Collante','Allanda','Rodriguez','Quintana');
            for ($i=0; $i < 200; $i++) { 
                $sql = "INSERT INTO alumnos (nombre, apellido, matricula) VALUES ('".$nombre[array_rand($nombre)]."','".$apellido[array_rand($apellido)]."','".$i."');";
                $dbconn->query($sql)->execute(); 
            }
    }
    /*
    $apellido = array(
        'Espinola','Cruz','Martinez','Lopez','Moreira','Paredes','Collante','Allanda','Rodriguez','Quintana'
    );
    $num2 = 10;
    $keys2 = array_rand($apellido,$num2);
    echo $apellido[$keys[0]]; prueba1*/

    function insertCursos($dbconn) {

        $cursos = array('matematica', 'informatica', 'quimica', 'biologia', 'castellano', 'trigonometria', 'fisica', 'aritmetica');
        $anos = array('2009', '2010', '2011', '2012', '2013', '2014', '2015');

        for ($i=0; $i < 50; $i++) { 
            for ($seccion=1; $seccion < 5; $seccion++) { 
                $sql = $dbconn->query("INSERT INTO cursos (nombre, ano, seccion) VALUES ('".$cursos[array_rand($cursos)]."','".$anos[array_rand($anos)]."','".$seccion."');")->execute(); 
            }
        }
    }

    function insertInscripciones($dbconn) {
    
        $queryAlumnos = $dbconn->query("SELECT * FROM alumnos");
        $alumnos = $queryAlumnos->fetchAll(PDO::FETCH_OBJ);

        $queryCursos = $dbconn->query("SELECT * FROM cursos");
        $cursos = $queryCursos->fetchAll(PDO::FETCH_OBJ);

            foreach ($alumnos as $alumno) {
                foreach ($cursos as $curso) {
                    $sql = $dbconn->query("INSERT INTO inscripciones (curso_id, alumno_id, fecha, activo) VALUES ('".$curso->id."', '".$alumno->id."', NOW(), true);")->execute();         
                }
            }
    }

    insertAlumnos($dbconn);
    insertCursos($dbconn);
    insertInscripciones($dbconn);

    /*--------------------------------------------------------------------------------------------*/
    echo "<h3>TOP 5 de los cursos con más incriptos- Nombre del curso – cantidad dealumnos</h3>";
    $qu = $dbconn->query("select c.nombre, count(ins.alumno_id) as cant
                        from inscripciones ins 
                        join cursos c on c.id = ins.curso_id 
                        group by c.nombre, ins.alumno_id 
                        order by cant asc
                        limit 5;");
    $column = $qu->fetchAll(PDO::FETCH_OBJ);
    echo "<table>";
        echo "<table>";
            echo "<tr>";
                echo "<td style='border: 1px solid black'> NOMBRE CURSO</td>";
                echo "<td style='border: 1px solid black'> CANTIDAD ALUMNOS</td>";
            echo "</tr>";

        foreach ($column as $value) 
        {
            echo "<tr>";
                echo "<td style='border: 1px solid black'> $value->cant </td>";
                echo "<td style='border: 1px solid black'> $value->nombre </td>";
            echo "</tr>";
        };
    echo "</table>";
    /*--------------------------------------------------------------------------------------------------*/

    echo "<h3>TOP 5 de los alumnos que están incriptos a más cursos. 
    Nombre y Apellido del alumno – matricula - cantidad de inscripciones</h3>";

    $qu = $dbconn->query("select a.nombre,a.apellido,a.matricula,count(curso_id) as cant
                    from inscripciones ins 
                    join cursos c on c.id = ins.curso_id
                    join alumnos a on a.id = ins.alumno_id 
                    group by a.nombre, a.apellido , a.matricula
                    order by  cant asc
                    limit 5;");
    $column = $qu->fetchAll(PDO::FETCH_OBJ);
    echo "<table>";
        echo "<table>";
            echo "<tr>";
                echo "<td style='border: 1px solid black'> NOMBRE </td>";
                echo "<td style='border: 1px solid black'> APELLIDOS </td>";
                echo "<td style='border: 1px solid black'> MATRICULA </td>";
                echo "<td style='border: 1px solid black'> CANTIDAD DE INSCRIPCIONES </td>";
            echo "</tr>";

        foreach ($column as $value) 
        {
            echo "<tr>";
                echo "<td style='border: 1px solid black'> $value->nombre </td>";
                echo "<td style='border: 1px solid black'> $value->apellido </td>";
                echo "<td style='border: 1px solid black'> $value->matricula </td>";
                echo "<td style='border: 1px solid black'> $value->cant </td>";
            echo "</tr>";
        };
    echo "</table>";

    /*--------------------------------------------------------------------------------*/
    
    echo "<h3>TOP 5 de las fechas con más incripciones. Fecha – Cantidad Inscripciones</h3>";
    $qu = $dbconn->query("select fecha , count(alumno_id) as cant
                    from inscripciones ins 
                    join cursos c on c.id = ins.curso_id
                    join alumnos a on a.id = ins.alumno_id 
                    group by fecha
                    order by  cant asc
                    limit 5;");
    $column = $qu->fetchAll(PDO::FETCH_OBJ);
    echo "<table>";
        echo "<table>";
            echo "<tr>";
                echo "<td style='border: 1px solid black'> FECHA </td>";
                echo "<td style='border: 1px solid black'> CANTIDAD INSCRIPCIONES </td>";
            echo "</tr>";

        foreach ($column as $value) 
        {
            echo "<tr>";
                echo "<td style='border: 1px solid black'> $value->fecha </td>";
                echo "<td style='border: 1px solid black'> $value->cant </td>";
            echo "</tr>";
        };
echo "</table>";

?>