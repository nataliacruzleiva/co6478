<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>EjercicioExtra</title>
</head>
<body>
<?php 

class vehiculo {

    private  $marca;
    private  $modelo;
    private  $anho;
    private $automoviles = array();

    public function __construct( $marca,  $modelo,  $anho, $automoviles) {
        $this->marca = $marca;
        $this->modelo = $modelo;
        $this->anho = $anho;
        $this->automoviles = $automoviles;
    }

    public function getMarca() {
        return $this->marca;
    }

    public function getModelo() {
        return $this->modelo;
    }

    public function getAnho() {
        return $this->anho;
    }

    public function getAutomoviles() {
        return $this->automoviles;
    }
}

    class automovil{

        private  $chapa;
        private  $color;
        private  $motor;

        public function __construct( $chapa,  $color,  $motor) {
            $this->chapa = $chapa;
            $this->color = $color;
            $this->motor = $motor;
        }

        public function getChapa() {
            return $this->chapa;
        }
        public function getColor() {
            return $this->color;
        }
        public function getMotor() {
            return $this->motor;
        }

    }

    $automovil1 = new automovil("BSO929", "GRIS", "1.1");
    $automovil2 = new automovil("BON300","Rojo","2.0");

    $vehiculos = new vehiculo("Hyundai", "Lexus", "2008", array($automovil1, $automovil2));

    $listaAutomoviles = $vehiculos->getAutomoviles();
    $marcaVehiculo = $vehiculos->getMarca();

    echo ("Las chapas de los vehiculos de la marca $marcaVehiculo son: \n");
    foreach ($listaAutomoviles as $key => $value) {
        $chapaVehiculo = $value->getChapa();
        echo (" - $chapaVehiculo \n");
    }
?>

</body>
</html>