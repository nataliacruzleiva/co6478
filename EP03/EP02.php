<?php
    $phpVersion = PHP_VERSION;
    $phpVersionId = PHP_VERSION_ID;
    $valorMaximoEnterosVersion = PHP_INT_MAX;
    $valorMaximoNombre = PHP_MAXPATHLEN;
    $osVersion = PHP_OS;
    $eol = PHP_EOL;
    $deftPath  = DEFAULT_INCLUDE_PATH;
    
    echo "Versión de PHP utilizada: $phpVersion <br>" ;
    echo "El id de la versión de PHP: $phpVersionId <br>";
    echo "El valor máximo soportado para enteros para esa versión: $valorMaximoEnterosVersion <br>";
    echo "Tamaño máximo del nombre de un archivo: $valorMaximoNombre <br>";
    echo "Versión del Sistema Operativo: $osVersion <br>";
    echo "Símbolo correcto de 'Fin De Línea' para la plataforma en uso: $eol <br>";
    echo "El include path por defecto: $deftPath <br>";
?>