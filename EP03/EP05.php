<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Heredoc</title>
</head>
<body>
    <?php
        $form = <<< html
            <form action="#" method="post">
                <input type="text" name="nombre" /><br/> <br/>
                <input type="text" name="apellido" /><br/> <br/>
                <input type="submit" name="submit" value="Submit" />
            </form>
        html;

        echo "<h3>Formulario</h3>";
        echo $form;
    ?>
</body>
</html>