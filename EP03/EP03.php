<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tabla Numeros</title>
    <style>
        tr:nth-child(even) {
            background-color: #D6EEEE;
        }
    </style>
</head>
<body>
    <?php
       $N = 25;
    ?>
       <table>
         <tr>
           <th>Tabla de numeros</th>
         </tr>
         <?php for ($i=0; $i < $N; $i++) { ?>
           <?php if($i%2==0){?>
            <tr>
                <td><?php echo $i ; ?></td>
            </tr>
         <?php } ?>
         <?php } ?>
</body>
</html>