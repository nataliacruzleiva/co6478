<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Calculadora</title>
</head>
<body>
    <form method="post" name="calculadora">
        <label>Ingrese valores</label><br>
        <div>
            <input type="text" name="num1"><br>
            <div>
                <select name="operaciones">
                    <option value="0">Sumar</option>
                    <option value="1">Restar</option>
                    <option value="2">Multiplicar</option> 
                    <option value="3">Dividir</option>
                </select>
            </div>
            <div>
                <input type="text" name="num2"><br>
            </div>
        </div><br>
        <div>
            <input type="submit" name="calcular" value="calcular"><br>
        </div>    
        </form>
    <?php
        if (isset($_REQUEST['calcular'])) {
            $num1 = $_REQUEST['num1'];
            $num2 = $_REQUEST['num2'];
            $op = $_REQUEST['operaciones'];
            
            switch ($op) {
                case 0:
                    $res = $num1+$num2;
                    echo $res;
                    break;
                case 1:
                    $res = $num1-$num2;
                    echo $res;
                    break;
                case 2:
                    $res = $num1*$num2;
                    echo $res;
                    break;
                case 3:
                    if($num2<>0){
                        $res = $num1/$num2;
                    }
                    echo $res;
                    break;
            }
        }
    ?>
</body>
</html>