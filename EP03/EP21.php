<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulario</title>
    <style>
        .error {color: #FF0000;}
    </style>
</head>
<body>
    <?php
        $nameErr = "";
        $name = "";
        $passwordErr = "";
        $password = "";
        $comment = "";
        $basescondiciones = "";

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (empty($_POST["name"])) {
              $nameErr = "Ingrese su usuario";
            } 
            
            if (empty($_POST["password"])) {
              $emailErr = "Ingrese su contraseña";
            }
        }
    ?>

    <h2>Desubscripción</h2>
    <p><span class="error">*required field</span></p>

    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">  
        <div> <!-- Un campo de Texto -->
            Usuario: <input type="text" name="name" value="<?php echo $name;?>">
            <span class="error">* <?php echo $nameErr;?></span>
        </div><br>

        <div> <!-- Un campo Password -->
            Contraseña: <input type="password" name="password" minlength="8">
            <span class="error">* <?php echo $passwordErr;?></span>
        </div><br>

        <div><!-- Un select Simple -->
           <label>Estás seguro que deseas desuscribirte?</label>
                <select name="cond" id="cond">
                  <option value="Si">Si</option>
                  <option value="No">No</option>
                </select>
        </div>

        <div> <!-- Un Checkbox -->
            <p>Si tienes un momento, por favor selecciona porque quieres desuscribirte</p>
            <div>
            <input type="checkbox" name="norecibir">
                <label for="norecibir">No quiero recibir más correos</label>
            </div>
            <div>
                <input type="checkbox" name="inapropiado">
                <label for="inapropiado">Los correos son inapropiados</label>
            </div>
            <div>
                <input type="checkbox" name="spam">
                <label for="spam">Los correos son spam</label>
            </div>
        </div><br>
        <div> 	<!-- Un Text Area -->
            <label>Comentanos cómo podemos mejorar!</label><br>
            <textarea name="comment" rows="5" cols="40"><?php echo $comment;?></textarea>
        </div><br>
        <div> <!-- Un Radio Button -->
            <input type="radio" name="basescondiciones">Acepto las bases y condiciones que conllevan esta accion
        </div><br>
        <div>
            <input type="submit" name="submit" value="Enviar">
        </div>
    </form>

    <?php
    	echo "<h2>Información enviada:</h2>";
    	echo "nombre: $name <br>";
    	echo "contraseña: $password <br>";
        echo "comentarios: $comment <br>"; 
    ?>
</body>
</html>